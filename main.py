#!/usr/bin/env python3

import argparse
import yaml
import sys
import time
import json

from shodan import Shodan


def shodan_search(api_key, search_string):
    api_session = Shodan(api_key)
    results = api_session.search(search_string)
    return results


def dump_json_to_file(json_data):
    current_time = int(time.time())
    try:
        with open(f"shodan-{current_time}.json", "w") as results_file:
            json.dump(json_data, results_file)
    except Exception as unexpected_exception:
        sys.exit(f"Unexpected exception: {unexpected_exception}")


def main():
    parser = argparse.ArgumentParser(description="Simple command line tool for querying Shodan.io.",
                                     usage="\npython3 ./main.py -c config.yaml -p 80 443 8080 -o Example.org "
                                           "-n 169.254.0.0/16\n"
                                           "python3 ./main.py -c config.yaml "
                                           "-s \"port:80,443,8080 org:Example.org netblock:169.254.0.0/16\"")
    api_group = parser.add_mutually_exclusive_group()
    parser.add_argument("-n", "--netblocks", nargs="*", help="Specify list of netblocks "
                                                             "to query Shodan for")
    parser.add_argument("-o", "--org", help="Specify organization to query Shodan for")
    parser.add_argument("-s", "--search_string", help="Specify explicit search string")
    parser.add_argument("-p", "--port", nargs="*", help="Specify list of ports to query for")

    # one of two methods to enter API, mutually exclusive parser group
    api_group.add_argument("-c", "--config", help="Specify YAML input config file")
    api_group.add_argument("-a", "--api_key", help="Specify API key (Can result in keys in log files, "
                                                   "config file recommended")

    args = parser.parse_args()

    # load designated config file, load API key from file
    if args.config:
        try:
            with open(args.config) as file:
                yaml_dict = yaml.load(file, Loader=yaml.FullLoader)
        except IOError as e:
            sys.exit(e)
        except Exception as e:
            sys.exit(f"Unexpected exception while opening file: {e}")
        if yaml_dict['shodan_api_key'] and yaml_dict['shodan_api_key'] != 'insert api key here':
            api_key = yaml_dict['shodan_api_key']
        else:
            sys.exit("API key not detected or detected as default. Exiting...")
    elif args.api_key:
        api_key = args.api_key
    else:
        sys.exit("Shodan requires an API key to search and download results via API.")

    search_string = ""

    if args.netblocks:
        search_string += f" net:{','.join(args.netblocks)}"

    if args.org:
        search_string += f" org:{args.org}"

    if args.port:
        search_string += f" port:{','.join(args.port)}"

    if args.search_string:
        search_string += f" {args.search_string}"

    if search_string == "":
        sys.exit("No search types set, re-run with s, n, p, or o options.")

    print(search_string)

    shodan_result = shodan_search(api_key, search_string)
    dump_json_to_file(shodan_result)


if __name__ == "__main__":
    main()
