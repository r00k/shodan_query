Script to query Shodan via APIs and dump results to json files.

```
usage:
python3 ./main.py -c config.yaml -p 80 443 8080 -o Example.org -n 169.254.0.0/16
python3 ./main.py -c config.yaml -s "port:80,443,8080 org:Example.org netblock:169.254.0.0/16"

Simple command line tool for querying Shodan.io.

optional arguments:
  -h, --help            show this help message and exit
  -n [NETBLOCKS ...], --netblocks [NETBLOCKS ...]
                        Specify list of netblocks to query Shodan for
  -o ORG, --org ORG     Specify organization to query Shodan for
  -s SEARCH_STRING, --search_string SEARCH_STRING
                        Specify explicit search string
  -p [PORT ...], --port [PORT ...]
                        Specify list of ports to query for
  -c CONFIG, --config CONFIG
                        Specify YAML input config file
  -a API_KEY, --api_key API_KEY
                        Specify API key (Can result in keys in log files, config file recommended
```
